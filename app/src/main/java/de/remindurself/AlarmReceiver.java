package de.remindurself;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;


import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


import de.remindurself.asynctasks.DBAddTask;
import de.remindurself.asynctasks.DBFetchOneTask;
import de.remindurself.models.Reminder;
import de.remindurself.orm.ReminderDB;

import static de.remindurself.AlarmService.ACTION_SET_LOCATION_ALARMS;
import static de.remindurself.AlarmService.ACTION_SET_TIME_ALARMS;
import static de.remindurself.AlarmService.TIME_ALARMS_SET;

/**
 * this BroadcastReceiver handles all triggered Alarms and receives the system boot event to reregister alarms
 */
public class AlarmReceiver extends BroadcastReceiver
        implements DBFetchOneTask.DBFetchOneListener, DBAddTask.DBAddCompleteListener {

    public static final String TAG = "AlarmReceiver";

    public static final String NOTIFICATION_CHANNEL_ID = "ALARM_RECEIVER_NOTIFICATION_CHANNEL";

    Context mContext;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.v(TAG, "onReceive action: " + intent.getAction());
        mContext = context;
        String action = intent.getAction();
        createNotificationChannel(context);
        if (action != null) {
            if (action.contains("SEND_NOTIFICATION")) {
                sendNotification(context, intent);


            } else if (action.equals("android.intent.action.BOOT_COMPLETED")
                    || action.equals("android.intent.action.QUICKBOOT_POWERON")
                    || action.equals("android.intent.action.ACTION_LOCKED_BOOT_COMPLETED")) {
                Log.i(TAG, "Boot onReceive action: " + action);

                //when receiving any of the above system broadcasts, re-register alarms in
                //AlarmService; because Android's AlarmManager service discards registered
                //alarms on shut down
                Intent reRegisterTimedAlarms = new Intent(context, AlarmService.class);
                reRegisterTimedAlarms.setAction(ACTION_SET_TIME_ALARMS);
                AlarmService.enqueueWork(context, reRegisterTimedAlarms);

                //unsure if the proximity alarms persist through a reboot; reset them to be sure.
                //documentation does not provide an answer
                Intent reRegisterLocationAlerts = new Intent(context, AlarmService.class);
                reRegisterLocationAlerts.setAction(ACTION_SET_LOCATION_ALARMS);
                AlarmService.enqueueWork(context, reRegisterLocationAlerts);
            }

        }
    }



    private void sendNotification(Context context, Intent intent){
        Log.v(TAG, "sendNotification()");
        long reminderId = intent.getLongExtra("id", -1);
        if(reminderId == -1){
            Log.e(TAG, "sendNotification() invalid reminder id");
            return;
        }

        //check if the Alarm has frequency assigned. If yes, register the alarm again.
        //AlarmManager has methods that automatically create repeating alarms, but they are all
        //inexact and the documentation recommends manually recreating repeating alarms when
        //exact triggers are needed.
        if(intent.getBooleanExtra("frequency", false)){
            //start process by getting the corresponding Reminder in the database.
            //receive result in onFetchComplete() further down
            new DBFetchOneTask(this).execute(reminderId);
        }

        //build Notification with Reminder title, description and an Intent for user interaction
        //with the notification
        String title = intent.getStringExtra("title");
        String description = intent.getStringExtra("description");
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID);
        builder.setContentTitle(title);
        builder.setContentText(description);
        builder.setSmallIcon(R.drawable.ic_alarm_primary_24dp);
        builder.setPriority(NotificationCompat.PRIORITY_HIGH);

        Intent notificationIntent = new Intent(context, DetailActivity.class);

        //use TaskStackBuilder to allow the user to press the back arrow to return to the
        //Main Activity after accessing the Detail view through a notification
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntentWithParentStack(notificationIntent);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        //have to create a unique Action for every PendingIntent because otherwise the PendingIntents would be
        //the same according to Intent.filterEquals which means the old PendingIntent gets overwritten
        //with the new Intent. This is a problem if there are two notifications displayed at the same time because
        //the PendingIntent of the first one would get overwritten with the second one, so now both
        //Notifications lead to the same Reminder upon interaction.
        notificationIntent.setAction(DetailActivity.ACTION_VIEW_REMINDER + System.currentTimeMillis());

        //The Reminder object is not present here, because Intents seem to lose parcel extras when passing
        //through the AlarmManager. Sending the id instead so the recipient can get the Reminder from
        //the database.
        notificationIntent.putExtra("id", reminderId);
        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setContentIntent(pendingIntent);

        Date currentDate = new Date();
        int id = Integer.parseInt(new SimpleDateFormat("ddHHmmssSS", Locale.GERMANY).format(currentDate));
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context);
        notificationManagerCompat.notify(id, builder.build());
    }


    @Override
    public void onFetchComplete(ReminderDB reminder) {
        //Retrieved reminder from database. Use DBAddTask to readd the adjusted Reminder with the same id.
        //The old one will be replaced because onconflictstrategy is set to replace

        long currentUnixTime = reminder.getDate();
        long interval = 0;
        switch (reminder.getFrequency()) {
            //I don't know how to access string resources from here; no getResources() outside an Activity
            case "Keine":
            case "":
                return; //if no frequency is set, do nothing
            case "Stündlich":
                //calculate seconds in hour
                interval = 60*60;
                break;
            case "Täglich":
                //seconds in a day
                interval = 24*60*60;
                break;
            case "Wöchentlich":
                //seconds in a week
                interval = 7*24*60*60;
                break;
            case "Monatlich":
                //seconds in a month
                interval = 30*24*60*60;
        }
        long newDate = currentUnixTime + interval;
        Reminder newReminder = new Reminder(reminder.getTitle(), newDate, reminder.getDescription(), reminder.getFrequency());
        newReminder.setAssignedTime(true);
        newReminder.setId(reminder.getReminderId());
        TIME_ALARMS_SET = false;
        new DBAddTask(this).execute(newReminder);

    }

    @Override
    public void onAddComplete() {
        //have to use a listener to be notified when the add operation is finished before reregistering
        //the alarms, because the service reads the database when registering alarms and it could
        //therefore theoretically execute before the object is added to the database and miss it.
        if(mContext != null) {
            Intent intent = new Intent(mContext, AlarmService.class);
            intent.setAction(ACTION_SET_TIME_ALARMS);
            AlarmService.enqueueWork(mContext, intent);
        } else {
            Log.e(TAG, "context is null, cannot reregister alarms");
        }
    }

    private void createNotificationChannel(Context context){
        //create NotificationChannel on API 26+
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            CharSequence name = "Ausgelöste Erinnerungen";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, name, importance);
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }
    }
}
