package de.remindurself;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.JobIntentService;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.Date;

import de.remindurself.common.RemindURselfApplication;
import de.remindurself.models.Reminder;
import de.remindurself.orm.ReminderDB;

/**
 * this JobIntentservice takes care of registering the alarms with the system.
 * The Android class AlarmManager discards registered alarms when the system is rebooted,
 * so this service has to be called when the AlarmReceiver receives a system boot broadcast
 * <p>
 * Using JobIntentService instead of IntentService, because since API 26, Android doesn't let you
 * launch background services without a prior foreground activity first. This app needs to
 * launch a background action that reregisters all alarms when the device is rebooted
 */
public class AlarmService extends JobIntentService {
    public static final String TAG = "AlarmService";

    public static final String ACTION_SET_TIME_ALARMS = "ALARMSERVICE_SET_TIME_ALARMS";
    public static final String ACTION_SET_LOCATION_ALARMS = "ALARMSERVICE_SET_LOCATION_ALARMS";

    public static final String ACTION_SEND_NOTIFICATION_TIMED = "ALARMRECEIVER_SEND_NOTIFICATION_TIMED";
    public static final String ACTION_SEND_NOTIFICATION_LOCATION = "ALARMRECEIVER_SEND_NOTIFICATION_LOCATION";

    public static final String ACTION_REMOVE_ALARM = "ALARMSERVICE_REMOVE_ALARM";

    //helper variables to keep track of alarm status throughout app
    public static boolean TIME_ALARMS_SET = true;
    public static boolean LOCATION_ALARMS_SET = true;


    public AlarmService() {
        super();
    }

    //static convenience method to simplify enqueueing work
    public static void enqueueWork(Context context, Intent work) {
        //check for location permissions here because we can still Toast here before going async,
        //so we can easily inform the user about missing permissions.
        if(work != null && work.getAction() != null && work.getAction().equals(ACTION_SET_LOCATION_ALARMS)) {
            //LocationManager.addProximityAlert() requires location permissions
            //The user is asked for permissions when choosing a location for a Reminder
            if (work.getAction().equals(ACTION_SET_LOCATION_ALARMS) && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (context.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED
                        && context.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
                    Log.e(TAG, "fetchAndRegisterProximityAlerts permissions not granted");
                    Toast.makeText(context, "Orts-basierte Erinnerungen wegen fehlenden Berechtigungen nicht gesetzt", Toast.LENGTH_LONG).show();
                    return;
                }
            }
        }
        if(work != null) {
            enqueueWork(context, AlarmService.class, 1, work);
        }
    }

    @Override
    protected void onHandleWork(@Nullable Intent intent) {
        String action = "";
        if (intent != null && intent.getAction() != null) {
            action = intent.getAction();
        }
        switch (action) {
            case "":
                Log.e(TAG, "onHandleIntent missing Intent action");
                break;
            case ACTION_SET_LOCATION_ALARMS:
                fetchAndregisterProximityAlerts();
                break;
            case ACTION_SET_TIME_ALARMS:
                fetchAndRegisterTimedReminders();
                break;
            case ACTION_REMOVE_ALARM:
                unRegisterAlarm(intent);
                break;
        }
    }


    //get Reminders from database and register the timed ones with the AlarmManager
    @SuppressWarnings("unchecked")
    private void fetchAndRegisterTimedReminders() {
        ArrayList<ReminderDB> reminders = (ArrayList) RemindURselfApplication.getAppDatabase().getReminderDao().queryAll();
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        Intent alarmIntent;
        TIME_ALARMS_SET = true;
        for (ReminderDB item : reminders) {
            //check if item has an assigned date that is in the future; otherwise no alarm is needed
            if (item.getAssignedTime() && item.getDate() > (new Date().getTime() / 1000)) {
                alarmIntent = new Intent(this, AlarmReceiver.class);

                //unique Action string per Reminder to avoid conflicts of PendingIntents.
                alarmIntent.setAction(ACTION_SEND_NOTIFICATION_TIMED + item.getReminderId());

                //sending a Parcel through the AlarmManager seems to not work; the Parcel gets lost
                //and returns null when attempting to unwrap later. Sending primitive types seperately instead
                alarmIntent.putExtra("id", item.getReminderId());
                alarmIntent.putExtra("title", item.getTitle());
                alarmIntent.putExtra("description", item.getDescription());
                if(!item.getFrequency().equals("Keine") && !item.getFrequency().equals("")){
                    alarmIntent.putExtra("frequency", true);
                }

                PendingIntent pendingIntent = PendingIntent.getBroadcast(this, (int) item.getReminderId(), alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                if (alarmManager != null) {
                    //all pendingintents will be received in the class AlarmReceiver
                    alarmManager.setExact(AlarmManager.RTC_WAKEUP, item.getDate() * 1000, pendingIntent);
                }
            }
        }
    }

    //Suppress missing permission check warning because we are already performing the check before going async
    @SuppressLint("MissingPermission")
    @SuppressWarnings("unchecked")
    private void fetchAndregisterProximityAlerts() {
        Log.v(TAG, "fetchandregisterproximityalert");
        ArrayList<ReminderDB> reminders = (ArrayList) RemindURselfApplication.getAppDatabase().getReminderDao().queryAll();
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        LOCATION_ALARMS_SET = true;
        Intent locationIntent;


        for (ReminderDB item : reminders) {
            if (item.getAssignedLocation()) {
                locationIntent = new Intent(this, AlarmReceiver.class);
                //unique Intent action string to avoid overwrite of PendingIntents
                locationIntent.setAction(ACTION_SEND_NOTIFICATION_LOCATION + item.getReminderId());
                locationIntent.putExtra("id", item.getReminderId());
                locationIntent.putExtra("title", item.getTitle());
                locationIntent.putExtra("description", item.getDescription());
                PendingIntent pendingIntent = PendingIntent.getBroadcast(this, (int) item.getReminderId(), locationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                if (locationManager != null) {
                    Log.d(TAG, "adding proximity alert for location " + item.getAddress());
                    //these pendingintents will be received by the class AlarmReceiver
                    locationManager.addProximityAlert(item.getLatitude(), item.getLongitude(), 250f, -1, pendingIntent);
                } else {
                    Log.e(TAG, "locationManager null");
                }
            }
        }
    }


    private void unRegisterAlarm(Intent intent) {
        Reminder reminder = Parcels.unwrap(intent.getParcelableExtra("reminder"));
        if (reminder == null) return;

        if (reminder.getAssignedLocation()) {

            //use LocationManager.removeProximityAlert after providing an identical PendingIntent as
            //when it was created.

            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            if(locationManager == null) return;
            Intent removeIntent = new Intent(this, AlarmReceiver.class);
            removeIntent.setAction(ACTION_SEND_NOTIFICATION_LOCATION + reminder.getId());
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this, (int) reminder.getId()
                    , removeIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            locationManager.removeProximityAlert(pendingIntent);

        } else if(reminder.getAssignedTime()){

            //use AlarmManager.cancel after providing an identical PendingIntent as when the alarm was
            //first created.

            AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
            if(alarmManager == null) return;
            Intent removeIntent = new Intent(this, AlarmReceiver.class);
            removeIntent.setAction(ACTION_SEND_NOTIFICATION_TIMED + reminder.getId());
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this, (int) reminder.getId()
                    , removeIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.cancel(pendingIntent);
        } else {
            Log.v(TAG, "unRegisterAlarm skipped; Reminder had no time or location");
        }
    }
}
