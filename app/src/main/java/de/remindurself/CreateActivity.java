package de.remindurself;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;

import de.remindurself.asynctasks.DBAddTask;
import de.remindurself.models.Reminder;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;


public class CreateActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String TAG = "CreateActivity";

    public static final int PLACES_AUTOCOMPLETE_REQUEST_CODE = 1;
    public static final int PERMISSION_REQUEST_CODE = 2;
    public static final int LOCATION_RESULT_REQUEST_CODE = 3;

    ScrollView scrollview;

    TextView title;
    TextView location;
    TextView time;
    TextView date;
    TextView description;
    TextView warning;
    TextView instructions_title;

    EditText etTitle;
    EditText etLocation;
    EditText etTime;
    EditText etDate;
    EditText etDescription;

    Button create;
    Button clear;
    Button clearLocation;

    LatLng selectedLatLng;

    String addressLine;

    ConstraintLayout timeCard;
    ConstraintLayout locationCard;
    ConstraintLayout frequencyCard;

    ToggleButton toggleFrequency;
    ToggleButton toggleTime;
    ToggleButton toggleLocation;

    Spinner frequencySpinner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_reminder);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        init();
        setToggleButtons();
        setCreateButton();
        setClearButton();
        setupLocationPicker();
    }

    private void init() {

        //initialising of the views
        scrollview = findViewById(R.id.sv_create);
        title = findViewById(R.id.create_tv_title);
        etTitle = findViewById(R.id.create_et_title);
        location = findViewById(R.id.create_tv_location);
        etLocation = findViewById(R.id.create_et_location);
        time = findViewById(R.id.create_tv_time);
        etTime = findViewById(R.id.create_et_time);
        date = findViewById(R.id.create_tv_date);
        etDate = findViewById(R.id.create_et_date);
        description = findViewById(R.id.create_tv_description);
        etDescription = findViewById(R.id.create_et_description);
        create = findViewById(R.id.create_bv_create);
        clear = findViewById(R.id.create_bv_clear);
        warning = findViewById(R.id.create_tv_warning);
        instructions_title = findViewById(R.id.create_tv_instructions_title);
        clearLocation = findViewById(R.id.create_bv_clear_location);

        //Toggle Button for the create Activity
        toggleLocation = findViewById(R.id.create_tb_reminder_location);
        toggleTime = findViewById(R.id.create_tb_reminder_time);
        toggleFrequency = findViewById(R.id.create_tb_frequency);
        frequencySpinner = findViewById(R.id.create_cv_spinner);

        //CardViews for the create Activity
        frequencyCard = findViewById(R.id.create_cv_frequency);
        locationCard = findViewById(R.id.create_cv_location);
        timeCard = findViewById(R.id.create_cv_time);

        //SpinnerAdapter
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.spinner_frequency, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        frequencySpinner.setAdapter(adapter);

        //Creating a calendar object and variables for the date- and time- Pickers
        Calendar calendar = Calendar.getInstance();
        int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
        int currentMinute = calendar.get(Calendar.MINUTE);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);


        //Creating the DatePicker --> called in etTimer OnClick-Listener
        final DatePickerDialog datePickerDialog = new DatePickerDialog(CreateActivity.this, (datePicker, year1, month1, day1) -> {
            //don't know why, but month is always 1 to low
            month1 += 1;

            //This part formats the output of the DatePicker and sets the EditText-Field
            if (month1 < 10) {
                if (day1 < 10) {
                    etDate.setText(String.format(Locale.GERMANY, "0%d.0%d.%d", day1, month1, year1));
                } else {
                    etDate.setText(String.format(Locale.GERMANY, "%d.0%d.%d", day1, month1, year1));
                }
            } else {
                if ((day1 < 10)) {
                    etDate.setText(String.format(Locale.GERMANY, "0%d.%d.%d", day1, month1, year1));
                } else {
                    etDate.setText(String.format(Locale.GERMANY, "%d.%d.%d", day1, month1, year1));
                }
            }
        }, year, month, day);


        //Creating the TimePicker --> called in etDate OnClick-Listener
        final TimePickerDialog timePickerDialog = new TimePickerDialog(CreateActivity.this, (timePicker, hourOfDay, minutes) -> {

            //This part formats the output of the TimePicker and sets the EditText-Field
            if (minutes >= 10) {
                if (hourOfDay >= 10) {
                    etTime.setText(String.format(Locale.GERMANY, "%d:%d", hourOfDay, minutes));
                } else {
                    etTime.setText(String.format(Locale.GERMANY, "0%d:%d", hourOfDay, minutes));
                }
            } else {
                if (hourOfDay >= 10) {
                    etTime.setText(String.format(Locale.GERMANY, "%d:0%d", hourOfDay, minutes));
                } else {
                    etTime.setText(String.format(Locale.GERMANY, "0%d:0%d", hourOfDay, minutes));
                }
            }
        }, currentHour, currentMinute, true);


        //This method prevents the keyboard to pop up
        etTime.setInputType(InputType.TYPE_NULL);
        //Using the OnClickListener Method, to show the TimePicker
        etTime.setOnClickListener(view -> timePickerDialog.show());
        //This method is necessary, so that the Picker is also shown, when the EditText is only selected
        etTime.setOnFocusChangeListener((view, b) -> {

            if (b) {
                timePickerDialog.show();
            }
        });

        //Setting the date input listener
        etDate.setOnClickListener(view -> datePickerDialog.show());
        //This method prevents the keyboard to pop up
        etDate.setInputType(InputType.TYPE_NULL);
        //This method is necessary, so that the Picker is also shown, when the EditText is only selected
        etDate.setOnFocusChangeListener((view, b) -> {
            if (b) {
                datePickerDialog.show();
            }
        });


        clearLocation.setOnClickListener(view -> {
            etLocation.setText("");
            selectedLatLng = null;
        });

    }

    public void setToggleButtons() {
        //Setting the toggle Button for the location segment
        toggleLocation.setOnCheckedChangeListener((compoundButton, isChecked) -> {

            if (isChecked) {
                checkPermissions();
                toggleTime.setChecked(false);
                locationCard.setVisibility(View.VISIBLE);
            } else {
                locationCard.setVisibility(View.GONE);
            }
        });

        //Setting the toggle Button for the time segment
        toggleTime.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (isChecked) {
                toggleLocation.setChecked(false);
                timeCard.setVisibility(View.VISIBLE);
            } else {
                timeCard.setVisibility(View.GONE);
            }
        });

        //Setting the toggle Button for the Frequency, within the time segment
        toggleFrequency.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (isChecked) {
                frequencyCard.setVisibility(View.VISIBLE);
            } else {
                frequencyCard.setVisibility(View.GONE);
            }
        });
    }

    public void setCreateButton() {
        //This method creates the onClickListener for the create Button
        create.setOnClickListener(view -> {


            //Reminder Object, which stores the created Reminder
            Reminder reminder;
            //Saving the users inputs
            String title = etTitle.getText().toString();
            String time = etTime.getText().toString();
            String date = etDate.getText().toString();
            String description = etDescription.getText().toString();

            //combined date and time as an unix timestamp
            long unixTime = dateTimeToLong(date, time);


            if (title.equals("")) {
                warning.setVisibility(TextView.VISIBLE);
                return;


            } else if (toggleTime.isChecked()) {

                if (time.equals("") || date.equals("")) {
                    Toast.makeText(this, "Datum UND Uhrzeit notwendig", Toast.LENGTH_LONG).show();
                    return;
                }
                reminder = new Reminder(title, unixTime, description, "");
                reminder.setAssignedTime(true);
                if (toggleFrequency.isChecked()) {
                    String frequency = frequencySpinner.getSelectedItem().toString();
                    reminder.setFrequency(frequency);
                }

            } else if (toggleLocation.isChecked()) {
                Location selectedLocation = new Location("");
                selectedLocation.setLatitude(selectedLatLng.latitude);
                selectedLocation.setLongitude(selectedLatLng.longitude);
                reminder = new Reminder(title, description, selectedLocation);
                reminder.setAssignedLocation(true);
                reminder.setAddress(addressLine);
            } else {
                reminder = new Reminder(title, description);
            }
            new DBAddTask().execute(reminder);


            //Intent back to the main activity
            Intent intent = new Intent(CreateActivity.this, MainActivity.class);
            startActivity(intent);

        });
    }

    public void setClearButton() {
        //Setting a button to clear all fields
        clear.setOnClickListener(view -> {
            etTitle.getText().clear();
            etLocation.getText().clear();
            etTime.getText().clear();
            etDate.getText().clear();
            etDescription.getText().clear();
            selectedLatLng = null;
            warning.setVisibility(TextView.INVISIBLE);
        });
    }

    private void setupLocationPicker() {
        Button buttonAddress = findViewById(R.id.create_bv_address);
        Button buttonMap = findViewById(R.id.create_bv_map);
        buttonAddress.setOnClickListener(this);
        buttonMap.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.create_bv_address) {
            List<Place.Field> fields = Arrays.asList(Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
            Intent placesIntent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields).build(this);
            startActivityForResult(placesIntent, PLACES_AUTOCOMPLETE_REQUEST_CODE);
        } else if (view.getId() == R.id.create_bv_map) {
            startLocationSelection();
        }
    }

    private void startLocationSelection() {

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Location lastLocation;
        Location bestLocation = null;

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            if (locationManager != null) {
                //getting last known location from all providers, because providers may return null
                List<String> providers = locationManager.getAllProviders();
                for (String provider : providers) {
                    lastLocation = locationManager.getLastKnownLocation(provider);
                    if (lastLocation != null && (bestLocation == null || lastLocation.getAccuracy() > bestLocation.getAccuracy())) {
                        bestLocation = lastLocation;
                    }
                }
                if (bestLocation == null) {
                    Log.d(TAG, "getLastKnownLocation return null");
                }
            }
        } else {
            Toast.makeText(this, "Für diese Funktion müssen Location-Berechtigungen gegeben sein", Toast.LENGTH_LONG).show();
            return;
        }
        Intent intent = new Intent(CreateActivity.this, MapActivity.class);
        if (bestLocation != null) {
            intent.putExtra("lat", bestLocation.getLatitude());
            intent.putExtra("lng", bestLocation.getLongitude());
        }
        startActivityForResult(intent, LOCATION_RESULT_REQUEST_CODE);
    }

    //receive the Place object returned when the user selects a place
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACES_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                assert data != null;
                Place place = Autocomplete.getPlaceFromIntent(data);
                selectedLatLng = place.getLatLng();

                //Place object already provides address, no need to reverse geocode this
                addressLine = place.getAddress();
                etLocation.setText(place.getAddress());
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                //handle error
                Status status = null;
                if (data != null) {
                    status = Autocomplete.getStatusFromIntent(data);
                }
                if (status != null) {
                    System.out.println(status.getStatusMessage());
                }
            } else if (resultCode == RESULT_CANCELED) {
                //cancelled
                Log.w(TAG, "onActivityResult result canceled");
            }
        } else if (requestCode == LOCATION_RESULT_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                assert data != null;
                if (data.hasExtra("lat") && data.hasExtra("lng")) {
                    double lat = data.getDoubleExtra("lat", 0);
                    double lng = data.getDoubleExtra("lng", 0);

                    selectedLatLng = new LatLng(lat, lng);
                    reverseGeocode();
                    etLocation.setText(addressLine);

                } else {
                    Log.e(TAG, "onActivityResult requestCode: " + requestCode + "; intent data missing");
                }
            } else {
                Toast.makeText(this, "Ortsauswahl abgebrochen", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void reverseGeocode() {
        //get address from latitude + longitude
        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.GERMANY);
        addressLine = "";
        try {
            List<Address> addressList = geocoder.getFromLocation(selectedLatLng.latitude, selectedLatLng.longitude, 1);
            Address address = addressList.get(0);
            addressLine = address.getAddressLine(0);
        } catch (Exception x) {
            Toast.makeText(this,
                    "Reverse Geocoding fehlgeschlagen. Fahre mit Längen- & Breitengrad fort",
                    Toast.LENGTH_LONG).show();

            //if the geocoding fails, the app can still continue as usual, it just can't show the
            //user the address to the point he just picked on the map. Will show Lag/Long instead
            addressLine = String.format(Locale.GERMANY, "Lat: %.5f --- Long: %.5f", selectedLatLng.latitude, selectedLatLng.longitude);
            x.printStackTrace();
        }
    }

    public long dateTimeToLong(String dateS, String time) {

        //In case the User creates an event without date and time set
        if (dateS.equals("") || time.equals("")) {
            return 0;
        }

        //Combining date and time
        String timestampString = dateS + "-" + time;

        DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy-HH:mm", Locale.GERMANY);

        try {
            Date timestamp = formatter.parse(timestampString);
            if (timestamp != null) {
                return timestamp.getTime() / 1000;
            }

        } catch (ParseException e) {
            return 0;
        }
        return 0;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        //only one item in this activity, no distinction necessary
        finish();
        return super.onOptionsItemSelected(item);
    }

    //check for permission and request them if they are not present
    private void checkPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_DENIED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_DENIED) {

            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_BACKGROUND_LOCATION}, PERMISSION_REQUEST_CODE);
        }
    }




}
