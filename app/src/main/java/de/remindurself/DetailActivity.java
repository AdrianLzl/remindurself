package de.remindurself;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import org.parceler.Parcels;

import de.remindurself.asynctasks.DBFetchOneTask;
import de.remindurself.asynctasks.DBRemoveTask;
import de.remindurself.models.Reminder;
import de.remindurself.orm.ReminderDB;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import static de.remindurself.AlarmService.ACTION_REMOVE_ALARM;


public class DetailActivity extends AppCompatActivity
        implements DBFetchOneTask.DBFetchOneListener
        , View.OnClickListener
        , DBRemoveTask.DBRemoveCompleteListener {
    public static final String TAG = "DetailActivity";
    public static final String ACTION_VIEW_REMINDER = "DETAILACTIVITY_ACTION_VIEW_REMINDER";

    ReminderDB reminder;

    TextView title;
    TextView address;
    TextView time;
    TextView date;
    TextView description;
    ScrollView scrollView;

    Button delete;

    ConstraintLayout layoutAddress;
    ConstraintLayout layoutDate;
    ConstraintLayout layoutDescription;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        init();
    }

    private void init() {
        scrollView = findViewById(R.id.sv_detail);
        title = findViewById(R.id.detail_tv_title);
        address = findViewById(R.id.detail_tv_adress);
        time = findViewById(R.id.detail_tv_time);
        date = findViewById(R.id.detail_tv_date);
        description = findViewById(R.id.detail_tv_description);
        delete = findViewById(R.id.detail_bv_delete);
        layoutAddress = findViewById(R.id.detail_cl_address);
        layoutDate = findViewById(R.id.detail_cl_date);
        layoutDescription = findViewById(R.id.detail_cl_description);

        //this Activity should always be called with an Intent that contains the ID of the Reminder
        //that should be displayed
        long id = getIntent().getLongExtra("id", -1);
        if (id == -1) {
            Log.e(TAG, "init() invalid Reminder id");
        }

        //Start AsyncTask to fetch the corresponding reminder. Pass this class as a listener for the result
        new DBFetchOneTask(this).execute(id);

    }

    @Override
    public void onFetchComplete(ReminderDB reminder) {
        this.reminder = reminder;

        //If the user deletes a reminder before clicking the notification to open it, this ReminderDB
        //will be null
        if(reminder == null){
            Toast.makeText(this, "Erinnerung nicht gefunden", Toast.LENGTH_LONG).show();
            return;
        }
        setUIValues(reminder);
    }

    private void setUIValues(ReminderDB reminder) {
        //format and assign UI values from the ReminderDB object

        title.setText(reminder.getTitle());
        address.setText(reminder.getAddress());
        Date date = new Date(reminder.getDate() * 1000);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMANY);
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.GERMANY);
        String formattedDate = dateFormat.format(date);
        String formattedTime = timeFormat.format(date);

        //make distinctions on the type of reminder and adjust the UI accordingly

        if (reminder.getFrequency() != null && !reminder.getFrequency().equals("") && !reminder.getFrequency().equals("Keine")) {
            time.setText(String.format("%s --- %s", formattedTime, reminder.getFrequency()));
        } else {
            time.setText(formattedTime);
        }
        this.date.setText(formattedDate);
        description.setText(reminder.getDescription());

        delete.setOnClickListener(this);

        if (reminder.getAssignedTime()) {
            layoutDate.setVisibility(View.VISIBLE);
        }
        if (reminder.getAssignedLocation()) {
            layoutAddress.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        //Delete Button is the only button on this page, no distinction needed

        //remove entry from database and send Intent to unregister the alarm to the AlarmService
        new DBRemoveTask(this).execute((int) reminder.getReminderId());
        Intent removeAlarm = new Intent(ACTION_REMOVE_ALARM);

        //service expects a Reminder object -> convert
        Reminder rem;
        if (reminder.getAssignedLocation()) {
            Location location = new Location("");
            location.setLongitude(reminder.getLongitude());
            location.setLatitude(reminder.getLatitude());
            rem = new Reminder(reminder.getTitle(), reminder.getDescription(), location);
            rem.setAddress(reminder.getAddress());
            rem.setAssignedLocation(true);
        } else if (reminder.getAssignedTime()) {
            rem = new Reminder(reminder.getTitle(), reminder.getDate(), reminder.getDescription(), reminder.getFrequency());
            rem.setAssignedTime(true);
        } else {
            rem = new Reminder(reminder.getTitle(), reminder.getDescription());
        }

        removeAlarm.putExtra("reminder", Parcels.wrap(rem));
        AlarmService.enqueueWork(this, removeAlarm);

        //go back to MainActivity after Reminder is deleted
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onDeleteComplete() {

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        //back arrow is the only Menu item in this activity, no distinction necessary
        finish();
        return super.onOptionsItemSelected(item);
    }
}
