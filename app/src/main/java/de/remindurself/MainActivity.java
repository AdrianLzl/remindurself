package de.remindurself;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import de.remindurself.asynctasks.DBFetchTask;
import de.remindurself.asynctasks.DBRemoveTask;
import de.remindurself.models.Reminder;

import de.remindurself.adapter.ReminderRecyclerAdapter;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.parceler.Parcels;

import java.util.ArrayList;

import static de.remindurself.AlarmService.ACTION_REMOVE_ALARM;
import static de.remindurself.AlarmService.ACTION_SET_LOCATION_ALARMS;
import static de.remindurself.AlarmService.ACTION_SET_TIME_ALARMS;
import static de.remindurself.AlarmService.LOCATION_ALARMS_SET;
import static de.remindurself.AlarmService.TIME_ALARMS_SET;




/**
 * This activity contains a list of all created reminders
 * This activity contains a button, which directs to the "CreateActivity"
 */
public class MainActivity extends AppCompatActivity
        implements ReminderRecyclerAdapter.ItemClickListener, DBFetchTask.DBFetchCompleteListener,
        DBRemoveTask.DBRemoveCompleteListener {

    RecyclerView reminderRecyclerView;
    FloatingActionButton addButton;
    ReminderRecyclerAdapter adapter;
    TextView emptyList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        init();
    }

    public void init() {
        reminderRecyclerView = findViewById(R.id.rlv_main);
        addButton = findViewById(R.id.fab_main);
        emptyList = findViewById(R.id.main_tv_empty);


        addButton.setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this, CreateActivity.class);
            startActivity(intent);
        });

        setupRecyclerView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //initiate a refresh of the List when resuming this activity
        new DBFetchTask(this).execute(0);
    }

    //initialize RecyclerView with its adapter
    private void setupRecyclerView() {

        //create adapter, pass list of reminders and register this class as a listener to listen for
        //clicks of items in the recyclerview
        ArrayList<Reminder> emptyList = new ArrayList<>();
        this.adapter = new ReminderRecyclerAdapter(this, emptyList);
        this.adapter.setClickListener(this);
        reminderRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        reminderRecyclerView.setAdapter(this.adapter);

        //start database fetch. AsyncTask notifies this Activity when it finishes
        new DBFetchTask(this).execute();
    }

    //interface method from interface ItemClickListener which is defined in the ReminderRecyclerAdapter class.
    //this Activity is registered as a Listener when the adapter is initialized, so it can be notified
    //when an item in the RecyclerView of reminders is clicked.
    @Override
    public void onItemClick(View view, int position, Reminder reminder) {
        Intent intent = new Intent(MainActivity.this, DetailActivity.class);
        intent.putExtra("id", reminder.getId());
        /*intent.putExtra("reminder", Parcels.wrap(reminder));*/
        startActivity(intent);
    }

    //Delete Element
    @Override
    public void onItemLongClick(View view, int position, Reminder reminder) {
        new DBRemoveTask(this).execute((int) reminder.getId());
        Intent removeAlarm = new Intent(ACTION_REMOVE_ALARM);
        removeAlarm.putExtra("reminder", Parcels.wrap(reminder));
        AlarmService.enqueueWork(this, removeAlarm);
    }

    //interface method from interface DBFetchCompleteListener in DBFetchTask AsyncTask.
    //Gets called from onPostExecute, when the database operation is finished, with the resulting list.
    @Override
    public void onFetchComplete(ArrayList<Reminder> reminders) {

        //show a TextView when no reminders exist in list
        emptyList.setVisibility(reminders.size() == 0 ? View.VISIBLE : View.INVISIBLE);

        adapter.setReminders(reminders);
        adapter.notifyDataSetChanged();

        //Enqueue work with the AlarmService to register all timed alarms, if they are not all set
        if (!TIME_ALARMS_SET) {

            Intent intent = new Intent(this, AlarmService.class);
            intent.setAction(ACTION_SET_TIME_ALARMS);
            AlarmService.enqueueWork(this, intent);
            TIME_ALARMS_SET = true;
        }
        //enqueue work with the AlarmService to register all location alarms, if they are not all set
        if (!LOCATION_ALARMS_SET) {
            Intent locationIntent = new Intent(this, AlarmService.class);
            locationIntent.setAction(ACTION_SET_LOCATION_ALARMS);
            AlarmService.enqueueWork(this, locationIntent);
            LOCATION_ALARMS_SET = true;
        }


    }

    @Override
    public void onDeleteComplete() {
        //initiate refresh of list when database entry was deleted, for updated list display
        new DBFetchTask(this).execute(0);
    }
}
