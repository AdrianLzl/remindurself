package de.remindurself;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleMap.OnMapClickListener, View.OnClickListener {
    GoogleMap map;
    double currentLat;
    double currentLng;
    LatLng currentLatLng;

    double selectedLat;
    double selectedLng;

    Button confirm;
    TextView lat;
    TextView lng;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        init();
    }

    private void init() {
        //display back arrow to return without selection
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        //The intent should contain lat and long of the current location, so the map can open
        //at the correct location
        Intent intent = getIntent();
        if (intent.hasExtra("lat") && intent.hasExtra("lng")) {
            currentLat = intent.getDoubleExtra("lat", -1);
            currentLng = intent.getDoubleExtra("lng", -1);
            currentLatLng = new LatLng(currentLat, currentLng);
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        confirm = findViewById(R.id.bt_map_confirm_location);
        confirm.setOnClickListener(this);
        lat = findViewById(R.id.tv_map_lat);
        lng = findViewById(R.id.tv_map_lng);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            setResult(RESULT_CANCELED);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        //set Listener for location selection
        map.setOnMapClickListener(this);
        //enable current location button
        map.setMyLocationEnabled(true);
        //move camera to current location, zoom in
        if (currentLatLng != null) {
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(currentLatLng, 15f);
            map.moveCamera(cameraUpdate);
        }

    }

    @Override
    public void onMapClick(LatLng latLng) {
        //set new Marker at clicked LatLng and present user a button to confirm selection
        map.clear();
        map.addMarker(new MarkerOptions().position(latLng).title("Marker"));
        selectedLat = latLng.latitude;
        selectedLng = latLng.longitude;
        confirm.setVisibility(View.VISIBLE);
        lat.setText(String.format("Latitude: %s", selectedLat));
        lng.setText(String.format("Longitude: %s", selectedLng));
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.bt_map_confirm_location) {
            Intent result = new Intent();
            result.putExtra("lat", selectedLat);
            result.putExtra("lng", selectedLng);
            setResult(RESULT_OK, result);
            finish();
        }
    }
}
