package de.remindurself.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import de.remindurself.R;
import de.remindurself.models.Reminder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class ReminderRecyclerAdapter extends RecyclerView.Adapter<ReminderRecyclerAdapter.ReminderViewholder> {
    private ArrayList<Reminder> reminders;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    public ReminderRecyclerAdapter(Context context, ArrayList<Reminder> list) {
        reminders = new ArrayList<>(list);
        mInflater = LayoutInflater.from(context);
    }

    //method to update the contained ArrayList
    public void setReminders(ArrayList<Reminder> reminders) {
        this.reminders = new ArrayList<>(reminders);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ReminderViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recycler_item_reminder, parent, false);
        return new ReminderViewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReminderViewholder holder, int position) {
        Reminder reminder = reminders.get(position);
        holder.setmItem(reminder);
    }

    @Override
    public int getItemCount() {
        return reminders.size();
    }

    class ReminderViewholder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        View view;
        TextView title;
        TextView date_location;
        TextView description;
        Reminder reminder;

        //send click event and adapter position to the registered listener (MainActivity)
        @Override
        public void onClick(View v) {
            if (mClickListener != null) mClickListener.onItemClick(v, getAdapterPosition(),
                    reminders.get(getAdapterPosition()));
        }

        //send long click event and adapter position to the registered listener (MainActivity).
        //used for deleting entries
        @Override
        public boolean onLongClick(View view) {
            if (mClickListener != null) mClickListener.onItemLongClick(view, getAdapterPosition(),
                    reminders.get(getAdapterPosition()));
            return false;
        }

        ReminderViewholder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            this.title = itemView.findViewById(R.id.item_tv_title);
            this.date_location = itemView.findViewById(R.id.main_tv_date_location);
            this.description = itemView.findViewById(R.id.item_tv_description);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        //assign values from Reminder object to the views. Get date from unix timestamp in database,
        //and format to dd.mm.yyyy String
        void setmItem(Reminder reminder) {
            this.reminder = reminder;
            title.setText(reminder.getTitle());

            //hide description EditText if no description is set
            if (reminder.getDescription().equals("")) {
                description.setHeight(0);
                view.findViewById(R.id.item_divider).setVisibility(View.INVISIBLE);
            } else {
                description.setText(reminder.getDescription());
            }

            //Format and set the time of the Alarm
            if (reminder.getAssignedTime()) {
                Date date = new Date(reminder.getDate() * 1000);
                SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm --- dd.MM.yyyy", Locale.GERMANY);
                String formattedDate = dateFormat.format(date);

                //make distinction if the Alarm has a frequency assigned
                if (!reminder.getFrequency().equals("") && !reminder.getFrequency().equals("Keine")) {
                    this.date_location.setText(String.format("Zeit: %s --- %s", formattedDate, reminder.getFrequency()));
                } else {
                    this.date_location.setText(String.format("Zeit: %s", formattedDate));
                }

                //set the color to red if the Alarm's date is in the past
                if (reminder.getDate() < new Date().getTime() / 1000) {
                    this.date_location.setTextColor(Color.RED);
                }

                //set the location if the Reminder has a location instead of a time
            } else if (reminder.getAssignedLocation()) {
                this.date_location.setText(String.format("Ort: %s", reminder.getAddress()));
            } else {
                this.date_location.setVisibility(View.GONE);
            }
        }
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position, Reminder reminder);

        void onItemLongClick(View view, int position, Reminder reminder);
    }
}
