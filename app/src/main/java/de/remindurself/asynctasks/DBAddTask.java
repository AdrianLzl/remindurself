package de.remindurself.asynctasks;

import android.os.AsyncTask;


import de.remindurself.common.RemindURselfApplication;
import de.remindurself.models.Reminder;
import de.remindurself.orm.ReminderDB;

import static de.remindurself.AlarmService.LOCATION_ALARMS_SET;
import static de.remindurself.AlarmService.TIME_ALARMS_SET;


public class DBAddTask extends AsyncTask<Reminder, Integer, Reminder> {

    private DBAddCompleteListener listener;

    public DBAddTask(DBAddCompleteListener listener) {
        this.listener = listener;
    }

    public DBAddTask() {
    }

    @Override
    protected Reminder doInBackground(Reminder... reminder) {

        Reminder r = reminder[0];

        ReminderDB reminderDB;

        //Creates a new ReminderDB Object matching the different constructors
        if (r.getAssignedLocation()) {
            reminderDB = new ReminderDB(r.getTitle(), r.getDescription(), r.getLocation().getLatitude(), r.getLocation().getLongitude());
            reminderDB.setAssignedLocation(true);
            reminderDB.setAddress(r.getAddress());
            LOCATION_ALARMS_SET = false; //signal that not all location based alarms have been registered with the LocationManager
        } else if (r.getAssignedTime()) {

            //if the ID is not 0, it means an existing alarm is being reregistered due to an assigned
            //frequency. Give the new ReminderDB the same primary key ID so it can overwrite the
            //old one and not create a duplicate.
            if (r.getId() != 0) {
                reminderDB = new ReminderDB(r.getId(), r.getTitle(), r.getDate(), r.getDescription(), r.getFrequency());
            } else {
                reminderDB = new ReminderDB(r.getTitle(), r.getDate(), r.getDescription(), r.getFrequency());
            }
            reminderDB.setAssignedTime(true);
            TIME_ALARMS_SET = false; //signal that not all time based alarms have been registered with the AlarmManager
        } else {
            reminderDB = new ReminderDB(r.getTitle(), r.getDescription());
        }

        //Adds the reminderDB Object into the database
        RemindURselfApplication.getAppDatabase().getReminderDao().insertOne(reminderDB);

        return r;
    }

    @Override
    protected void onPostExecute(Reminder reminder) {
        super.onPostExecute(reminder);
        if (listener != null) {
            listener.onAddComplete();
        }
    }

    public interface DBAddCompleteListener {
        void onAddComplete();
    }
}
