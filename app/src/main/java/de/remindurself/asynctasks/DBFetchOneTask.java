package de.remindurself.asynctasks;

import android.os.AsyncTask;

import de.remindurself.common.RemindURselfApplication;
import de.remindurself.orm.ReminderDB;

/** Simple AsyncTask to retrieve a single entry of the database*/
public class DBFetchOneTask extends AsyncTask<Long, Integer, ReminderDB> {
    private DBFetchOneListener listener;

    public DBFetchOneTask(DBFetchOneListener listener) {
        this.listener = listener;
    }

    @Override
    protected ReminderDB doInBackground(Long... longs) {
        return RemindURselfApplication.getAppDatabase().getReminderDao().queryOne(longs[0]);
    }

    @Override
    protected void onPostExecute(ReminderDB reminderDB) {
        super.onPostExecute(reminderDB);
        listener.onFetchComplete(reminderDB);
    }

    public interface DBFetchOneListener {
        void onFetchComplete(ReminderDB reminder);
    }
}
