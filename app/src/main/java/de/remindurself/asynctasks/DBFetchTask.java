package de.remindurself.asynctasks;

import android.location.Location;
import android.os.AsyncTask;

import de.remindurself.models.Reminder;
import de.remindurself.common.RemindURselfApplication;
import de.remindurself.orm.ReminderDB;

import java.util.ArrayList;
import java.util.List;

/** AsyncTask to get all entries of the database */
public class DBFetchTask extends AsyncTask<Integer, Integer, ArrayList<Reminder>> {
    private DBFetchCompleteListener listener;

    public DBFetchTask(DBFetchCompleteListener listener) {
        this.listener = listener;
    }

    @Override
    protected ArrayList<Reminder> doInBackground(Integer... integers) {
        List<ReminderDB> reminderDBS = RemindURselfApplication.getAppDatabase().getReminderDao().queryAll();
        ArrayList<Reminder> reminders = new ArrayList<>();
        for (ReminderDB item : reminderDBS) {
            Reminder reminder;

            //for each entry in the database, create a corresponding Reminder object of the correct type

            if (item.getAssignedLocation()) {
                Location location = new Location("");
                location.setLatitude(item.getLatitude());
                location.setLongitude(item.getLongitude());
                reminder = new Reminder(item.getTitle(), item.getDescription(), location);
                reminder.setAssignedLocation(true);
                reminder.setAddress(item.getAddress());
            } else if (item.getAssignedTime()) {
                reminder = new Reminder(item.getTitle(), item.getDate(), item.getDescription(), item.getFrequency());
                reminder.setAssignedTime(true);
            } else {
                reminder = new Reminder(item.getTitle(), item.getDescription());
            }
            reminder.setId(item.getReminderId());
            reminders.add(reminder);
        }
        return reminders;
    }

    @Override
    protected void onPostExecute(ArrayList<Reminder> reminders) {
        super.onPostExecute(reminders);
        //pass reminders back to the MainActivity
        listener.onFetchComplete(reminders);
    }

    public interface DBFetchCompleteListener {
        void onFetchComplete(ArrayList<Reminder> reminders);
    }
}
