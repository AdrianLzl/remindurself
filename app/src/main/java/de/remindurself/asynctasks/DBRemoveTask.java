package de.remindurself.asynctasks;

import android.os.AsyncTask;

import de.remindurself.common.RemindURselfApplication;

/** Simple AsyncTask to remove a single database entry by the primary key ID*/
public class DBRemoveTask extends AsyncTask<Integer, Integer, Integer> {
    private DBRemoveCompleteListener listener;

    public DBRemoveTask(DBRemoveCompleteListener listener) {
        this.listener = listener;
    }

    @Override
    protected Integer doInBackground(Integer... integers) {
        RemindURselfApplication.getAppDatabase().getReminderDao().deleteOne((long) integers[0]);
        return null;
    }

    @Override
    protected void onPostExecute(Integer integer) {
        listener.onDeleteComplete();
    }

    public interface DBRemoveCompleteListener {
        void onDeleteComplete();
    }
}
