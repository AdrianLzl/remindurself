package de.remindurself.common;

import android.app.Application;

import androidx.room.Room;

import com.google.android.libraries.places.api.Places;

import de.remindurself.R;
import de.remindurself.orm.AppDatabase;

/**
 * Class that can be used to ensures the same database instance is accessed every time, because
 * every instance of a Room database is fairly resource-expensive.
 */
public class RemindURselfApplication extends Application {
    private static AppDatabase appDatabase;

    @Override
    public void onCreate() {
        super.onCreate();
        appDatabase = Room.databaseBuilder(this, AppDatabase.class, "reminders").build();
        initPlaces();
    }

    public static AppDatabase getAppDatabase() {
        return appDatabase;
    }


    //initialise Google Place service. Requires an API key from Google.
    //storing plain text API keys directly in the code is usually bad practice, but this is a free trial code
    //and the app won't be published.
    //Places API is required to find addresses and locations with autocomplete functionality in a search box
    private void initPlaces() {
        Places.initialize(this, getResources().getString(R.string.key_google_apis_android));
    }
}
