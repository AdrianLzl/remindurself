package de.remindurself.models;

import android.location.Location;

import org.parceler.Parcel;

/**
 * Basic model for a Reminder object
 */
@Parcel
public class Reminder {

    //this id corresponds with the primary key id in the database. Useful for delete operations
    long id;

    String title;
    String address;
    long date;
    String frequency;
    String description;

    Location location;

    boolean assignedLocation;

    boolean assignedTime;

    Reminder() {
    }

    /** Three constructors for the three types of Reminders within the app:
     * - No time or location
     * - Timed Alarm
     * - Location-based alarm
     */

    public Reminder(String title, long date, String description, String frequency) {
        this.title = title;
        this.date = date;
        this.description = description;
        this.frequency = frequency;
    }

    public Reminder(String title, String description, Location location) {

        this.title = title;
        this.description = description;
        this.location = location;
    }

    public Reminder(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getTitle() {
        return this.title;
    }

    public String getAddress() {
        return this.address;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    public long getDate() {
        return this.date;
    }

    public Location getLocation() {
        return location;
    }

    public void setAssignedLocation(boolean location) {
        this.assignedLocation = location;
    }

    public boolean getAssignedLocation() {
        return assignedLocation;
    }

    public void setAssignedTime(boolean time) {
        this.assignedTime = time;
    }

    public boolean getAssignedTime() {
        return assignedTime;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
