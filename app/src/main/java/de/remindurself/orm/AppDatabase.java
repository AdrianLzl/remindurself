package de.remindurself.orm;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {ReminderDB.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract ReminderDao getReminderDao();
}
