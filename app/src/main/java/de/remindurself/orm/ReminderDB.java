package de.remindurself.orm;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import org.parceler.Parcel;

@Parcel
@Entity(tableName = "reminders")
public class ReminderDB {

    @PrimaryKey(autoGenerate = true)
    long reminderId;

    @ColumnInfo(name = "title")
    String title;

    @ColumnInfo(name = "address")
    String address;

    @ColumnInfo(name = "date")
    long date;

    @ColumnInfo(name = "description")
    String description;

    double latitude;

    double longitude;

    boolean assignedLocation;

    boolean assignedTime;

    String frequency;


    ReminderDB() {
    }

    @Ignore
    //Constructor for a Reminder only by location
    public ReminderDB(String title, String description, double lat, double lng) {

        this.title = title;
        this.description = description;
        this.latitude = lat;
        this.longitude = lng;
    }

    @Ignore
    public ReminderDB(String title, String description) {
        this.title = title;
        this.description = description;
    }

    @Ignore
    //constructor for a Reminder only by date
    public ReminderDB(String title, long date, String description, String frequency) {

        this.title = title;
        this.date = date;
        this.description = description;
        this.frequency = frequency;

    }

    @Ignore
    //constructor for a Reminder only by date
    public ReminderDB(long id, String title, long date, String description, String frequency) {
        this.reminderId = id;
        this.title = title;
        this.date = date;
        this.description = description;
        this.frequency = frequency;

    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public void setAssignedLocation(boolean location) {
        this.assignedLocation = location;
    }

    public boolean getAssignedLocation() {
        return assignedLocation;
    }

    public void setAssignedTime(boolean time) {
        this.assignedTime = time;
    }

    public boolean getAssignedTime() {
        return assignedTime;
    }

    public long getReminderId() {
        return this.reminderId;
    }

    public String getTitle() {
        return this.title;
    }

    public long getDate() {
        return this.date;
    }

    public String getDescription() {
        return this.description;
    }


    void setReminderId(long reminderId) {
        this.reminderId = reminderId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getLatitude() {
        return latitude;
    }

    void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}
