package de.remindurself.orm;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface ReminderDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertOne(ReminderDB reminder);

    @Query("SELECT * FROM reminders")
    List<ReminderDB> queryAll();

    @Query("SELECT * FROM reminders WHERE reminderId = :id")
    ReminderDB queryOne(Long id);

    @Query("DELETE FROM reminders WHERE reminderId = :id")
    void deleteOne(Long id);
}
